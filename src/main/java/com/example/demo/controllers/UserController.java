package com.example.demo.controllers;

import com.example.demo.dao.UserRepository;
import com.example.demo.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public User save(@RequestBody  User user) {
        return userRepository.save(user);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public User getUser(@PathVariable long id) {
        return userRepository.findEntity(id);
    }

}
