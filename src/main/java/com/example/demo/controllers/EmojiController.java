package com.example.demo.controllers;

import com.example.demo.entities.Emoji;
import com.example.demo.services.EmojiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gallery/")
public class EmojiController {
    private final EmojiService emojiService;

    @Autowired
    public EmojiController(EmojiService emojiService) {
        this.emojiService = emojiService;
    }

    @PostMapping("upload")
    public void upload(@RequestParam long userId, @RequestBody Emoji emoji) {
        emojiService.uploadEmoji(userId, emoji);
    }

    @GetMapping("find")
    @ResponseBody
    public List<Emoji> findByUser(@RequestParam long userId) {
        return emojiService.findByUserId(userId);
    }

    @GetMapping("findAll")
    @ResponseBody
    public Page<Emoji> findAll(@RequestParam long userId, @RequestParam int page, @RequestParam int size) {
        PageRequest pageable = PageRequest.of(page, size);
        return emojiService.findAll(userId, pageable);
    }

}
