package com.example.demo.enums;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public enum UserType {

    FREE(5, List.of(EmojiType.FREE)),
    PREMIUM(100, Arrays.asList(EmojiType.FREE, EmojiType.PREMIUM)),
    BUSINESS(Integer.MAX_VALUE, Arrays.asList(EmojiType.values()));

    private final int emojiUploadLimit;
    private final Collection<EmojiType> allowedEmojis;

    UserType(Integer limit, Collection<EmojiType> allowedEmojies) {
        this.emojiUploadLimit = limit;
        this.allowedEmojis = allowedEmojies;
    }

    public int getEmojiUploadLimit() {
        return emojiUploadLimit;
    }

    public Collection<EmojiType> getAllowedEmojis() {
        return allowedEmojis;
    }
}
