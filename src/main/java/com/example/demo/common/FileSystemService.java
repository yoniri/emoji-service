package com.example.demo.common;

import java.io.File;

public interface FileSystemService {

    // Not Implemented
    void save(String path);
    String get(String path);
}
