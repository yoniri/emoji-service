package com.example.demo.common;

public class EntityNotFoundException extends RuntimeException{

    public EntityNotFoundException(Long id){
        super("Could not find id " + id);
    }
}
