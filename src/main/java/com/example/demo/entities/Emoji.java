package com.example.demo.entities;

import com.example.demo.enums.EmojiType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "emoji")
@Data
public class Emoji extends AbstractEntity {

    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "path")
    private String path;
    @Column(name = "is_active", nullable = false)
    private Boolean active;
    @Column(name = "display_order")
    private BigInteger displayOrder;
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private EmojiType type;
    @Column(name = "user_id", nullable = false)
    @JsonIgnore
    private Long userId;
}


