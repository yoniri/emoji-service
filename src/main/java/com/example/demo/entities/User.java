package com.example.demo.entities;

import com.example.demo.enums.UserType;
import lombok.Data;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "user")
@Data
public class User extends AbstractEntity {

    @Column(name = "name")
    private String name;
    @Column(name = "email", unique = true)
    private String email;
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserType userType;
    @Column(name = "emojis")
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "userId")
    private Set<Emoji> emojis;

}
