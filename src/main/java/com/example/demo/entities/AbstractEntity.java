package com.example.demo.entities;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Service
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "create_user_id", nullable = false)
    protected Integer createUserId = 1;//placeholder

    @Column(name = "date_created", nullable = false)
    @CreationTimestamp
    protected Date dateCreate;

    @Column(name = "update_user_id", nullable = false)
    protected Integer updateUserId = 1;//placeholder

    @Column(name = "last_updated", nullable = false)
    @UpdateTimestamp
    protected Date lastUpdated;

    public Long getId() {
        return id;
    }

}

