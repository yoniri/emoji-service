package com.example.demo.services;

import com.example.demo.common.FileSystemService;
import com.example.demo.dao.EmojiRepository;
import com.example.demo.dao.UserRepository;
import com.example.demo.entities.Emoji;
import com.example.demo.entities.User;
import com.example.demo.enums.UserType;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
public class EmojiService {

    private final static Long SYSTEM_USER_ID = 1L;
    private final static String USER_EMOJIS_CACHE = "userEmojis";
    private final static String TOP_EMOJIS_CACHE = "topEmojis";
    private final static String TOP_EMOJIS_KEY = "top100";

    private final EmojiRepository emojiRepository;
    private final UserRepository userRepository;
    private final FileSystemService fileSystemService;
    private final CacheManager cacheManager;

    public EmojiService(EmojiRepository emojiRepository, UserRepository userRepository, FileSystemService fileSystemService, CacheManager cacheManager) {
        this.emojiRepository = emojiRepository;
        this.userRepository = userRepository;
        this.fileSystemService = fileSystemService;
        this.cacheManager = cacheManager;
    }

    public void uploadEmoji(long userId, Emoji emoji) {
        final User user = userRepository.findEntity(userId);
        emoji.setUserId(userId);
        int uploadLimit = user.getUserType().getEmojiUploadLimit();
        if (user.getEmojis().size() > uploadLimit) {
            throw new RuntimeException(String.format("Upload Limit Exceeded For User %s", user.getEmail()));
        }
        //dummy write file
        fileSystemService.save(emoji.getPath());
        emojiRepository.save(emoji);
        evictUserCache(userId);
        if (userId == SYSTEM_USER_ID) {
            evictTopCache();
        }
    }

    @Cacheable(value = USER_EMOJIS_CACHE, key = "#userId")
    public List<Emoji> findByUserId(long userId) {
        List<Emoji> emojis = emojiRepository.findByUserId(userId);
        //dummy read file
        emojis.forEach(e -> e.setPath(fileSystemService.get(e.getPath())));
        return emojis;
    }

    public Page<Emoji> findAll(@RequestParam long userId, PageRequest pageable) {
        UserType userType = userRepository.findEntity(userId).getUserType();
        if (pageable.getPageNumber() == 0) {
            List<Emoji> emojis = getPopularEmojis();
            return new PageImpl<>(emojis.subList(0, emojis.size()), pageable, emojis.size());
        }
        return emojiRepository.findByTypeInAndUserIdAndActiveIsTrueOrderByDisplayOrder(userType.getAllowedEmojis(), SYSTEM_USER_ID, pageable);
    }

    private List<Emoji> getPopularEmojis() {
        Cache cache = cacheManager.getCache(TOP_EMOJIS_CACHE);
        if (cache != null) {
            Cache.ValueWrapper valueWrapper = cache.get(TOP_EMOJIS_KEY);
            if (valueWrapper != null && valueWrapper.get() instanceof List) {
                return (List<Emoji>) valueWrapper.get();
            }
        }
        List<Emoji> emojis = emojiRepository.findTopEmojisForUserOneLimitedTo(100);
        // Dummy read file
        emojis.forEach(e -> e.setPath(fileSystemService.get(e.getPath())));

        if (cache != null) {
            cache.put(TOP_EMOJIS_KEY, emojis);
        }
        return emojis;
    }

    private void evictTopCache() {
        Cache cache = cacheManager.getCache(TOP_EMOJIS_CACHE);
        if (cache != null) {
            cache.invalidate();
        }
    }

    private void evictUserCache(long userId) {
        Cache cache = cacheManager.getCache(USER_EMOJIS_CACHE);
        if (cache != null) {
            cache.evictIfPresent(userId);
        }
    }

}
