package com.example.demo.dao;

import com.example.demo.common.EntityNotFoundException;
import com.example.demo.entities.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.web.bind.annotation.CrossOrigin;



@NoRepositoryBean
@CrossOrigin
public interface IBaseRepository<T extends AbstractEntity> extends JpaRepository<T, Long> {

    default T findEntity(Long id) {
        return findById(id).orElseThrow(() -> new EntityNotFoundException(id));
    }

}
