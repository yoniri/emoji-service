package com.example.demo.dao;


import com.example.demo.entities.Emoji;
import com.example.demo.enums.EmojiType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface EmojiRepository extends IBaseRepository<Emoji> {

    List<Emoji> findByUserId(Long userId);

    Page<Emoji> findByTypeInAndUserIdAndActiveIsTrueOrderByDisplayOrder(Collection<EmojiType> emojiTypes, Long userId, Pageable pageable);

    @Query("SELECT e FROM Emoji e WHERE e.type = 'FREE' AND e.userId = 1 ORDER BY e.displayOrder ASC")
    List<Emoji> findTopEmojisForUserOneLimitedTo(@Param("limit") int limit);

}
