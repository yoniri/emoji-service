package com.example.demo.dao;

import com.example.demo.entities.User;


public interface UserRepository extends IBaseRepository<User> {
}
