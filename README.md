# Emoji Service Project

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Java Development Kit (JDK) 8 or later
- Spring Boot and Spring Data JPA
- Redis for caching
- MySQL for database
- Apache Maven for building the project

## Project Setup

1. Run Redis and MySQL

2. Run the following SQL to create DB and User:
   
   ```shell
   -- Create DB and User:
   CREATE DATABASE db_name;

   -- Create the user and grant privileges on the database
   CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
   GRANT ALL PRIVILEGES ON db_name.* TO 'username'@'localhost';
    ```

4. Clone the project repository to your local machine.

5. Open the project in your preferred Integrated Development Environment (IDE).

6. Update the application configuration in `application.properties`:

    - Configure the MySQL database settings.
    - Configure the Redis cache settings.

7. Build the project using Apache Maven:

    ```shell
    mvn clean install
    ```

8. Run the project:

    ```shell
     java -jar target/demo-0.0.1-SNAPSHOT.jar
    ```

9. Your Emoji Service should now be up and running.



## System Diagarm

![UML Diagram](https://lucid.app/publicSegments/view/8bf7029a-474a-4053-811b-3092f309c549/image.png)


